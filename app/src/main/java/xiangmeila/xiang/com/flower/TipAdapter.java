package xiangmeila.xiang.com.flower;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

/**
 * 提醒浇水具体
 */
public class TipAdapter extends BaseAdapter {

    private List<Flower> stuList;
    private List<Tip> tips;
    private LayoutInflater inflater;
    private int b;
    private int i;

    public TipAdapter() {}

    public TipAdapter(List<Flower> stuList, List<Tip> tips, Context context) {
        this.stuList = stuList;
        this.tips = tips;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return stuList==null?0:stuList.size();
    }

    @Override
    public Flower getItem(int position) {
        return stuList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //加载布局为一个视图
        View view=inflater.inflate(R.layout.item_tip,null);
        Flower student=getItem(position);
        //在view视图中查找id为image_photo的控件

        TextView tv_tip_name= (TextView) view.findViewById(R.id.tv_tip_name);
        TextView tv_tip_date= (TextView) view.findViewById(R.id.tv_tip_date);

        tv_tip_name.setText(student.getName());
        try {
            i = TimeUtils.daysBetween(TimeUtils.todayYyyyMmDd(),tips.get(position).getDate());
            Log.d("TipAdapter", tips.get(position).getDate());
            Log.d("TipAdapter", TimeUtils.todayYyyyMmDd());
            Log.d("TipAdapter", "i:" + i);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        /**
         * 得到正天数，并提醒
         */
        b = Math.abs(i);
        if(b==0) {
            tv_tip_date.setText("该浇水了哦");
        }else {
            tv_tip_date.setText("距离浇水还有"+b+"天");
        }
        return view;
    }
}
