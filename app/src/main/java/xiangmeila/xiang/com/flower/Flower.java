package xiangmeila.xiang.com.flower;

public class Flower {

    private  String  name;
    private  String   function;
    private  String cultivation;
    private  int   img;
    private  int   water;

    public Flower(String name, String function, String cultivation, int img, int water) {
        this.name = name;
        this.function = function;
        this.cultivation = cultivation;
        this.img = img;
        this.water = water;
    }

    public int getWater() {
        return water;
    }

    public void setWater(int water) {
        this.water = water;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getCultivation() {
        return cultivation;
    }

    public void setCultivation(String cultivation) {
        this.cultivation = cultivation;
    }
}
