package xiangmeila.xiang.com.flower;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class FlowerAdapter extends BaseAdapter {

    private List<Flower> stuList;
    private LayoutInflater inflater;
    public FlowerAdapter() {}

    public FlowerAdapter(List<Flower> stuList,Context context) {
        this.stuList = stuList;
        this.inflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return stuList==null?0:stuList.size();
    }

    @Override
    public Flower getItem(int position) {
        return stuList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //加载布局为一个视图
        View view=inflater.inflate(R.layout.item_business,null);
        Flower student=getItem(position);
        //在view视图中查找id为image_photo的控件
        ImageView image_photo= (ImageView) view.findViewById(R.id.im_business_show);
        TextView tv_name= (TextView) view.findViewById(R.id.tv_business_show);
        image_photo.setImageResource(student.getImg());
        tv_name.setText(student.getName());
        return view;
    }
}
