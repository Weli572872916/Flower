package xiangmeila.xiang.com.flower;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 */
public class FragmentCategory extends Fragment {
    ListView listView;
    View view;
    FlowerAdapter flowerAdapter;
    List<Flower> list = new ArrayList<>();
    List<String> stringList = new ArrayList<>();
    SqlHelper dbHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_category, container, false);
        initData();
        return view;
    }

    /**
     * 初始化
     */
    private void initData() {

        list.clear();
        stringList.clear();
        dbHelper = new SqlHelper(getActivity());
        listView = view.findViewById(R.id.lv_home_live);

        stringList.addAll(findAll());
//判断是否种植
        for (int i = 0; i < MyApp.flowerList.size(); i++) {
            for (int i1 = 0; i1 < stringList.size(); i1++) {
                if (stringList.get(i1).equals(MyApp.flowerList.get(i).getName())) {
                    list.add(MyApp.flowerList.get(i));
                }
            }
        }
        //设置列表数据
        flowerAdapter = new FlowerAdapter(list, getActivity());
        listView.setAdapter(flowerAdapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                DialogUtils.showDialog(getActivity(), "提示", "是否要移除改种植", "取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                }, "确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Flower flower = list.get(position);
                        /**
                         * 判断是否种植并种植
                         */

                        if (delete(flower) > 0) {
                            Toast.makeText(getActivity(), "删除种植成功", Toast.LENGTH_SHORT).show();
                            initData();
                        }


                    }
                });
                return false;
            }
        });
    }

    private int delete(Flower flower) {
        Log.d("FragmentCategory", "flower:" + flower);
        int i = 0;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        i = db.delete(SqlHelper.TABLE_NAME, SqlHelper.FLOWER_NAME + "= ?", new String[]{flower.getName()});
        db.close();

        return i;
    }

    /*
    查询已种的
     */
    public List<String> findAll() {
        List<String> persons = null;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (db.isOpen()) {
            persons = new ArrayList<String>();
            Cursor cursor = db.query(SqlHelper.TABLE_NAME, null, null, null, null, null, null);
            while (cursor.moveToNext()) {
                persons.add(cursor.getString(cursor.getColumnIndex("name")));
            }
            cursor.close();
            db.close();
        }
        return persons;
    }


}
