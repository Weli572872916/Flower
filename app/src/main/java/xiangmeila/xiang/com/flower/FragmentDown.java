package xiangmeila.xiang.com.flower;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class FragmentDown extends Fragment {
    ListView listView;
    View view;
    CustomerAdapter customerAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_down, container, false);
        initData();
        return view;
    }

    /**
     * 初始化
     */
    private void initData() {
        listView = view.findViewById(R.id.lv_home_order);

//        查询所有花,并显示
        customerAdapter = new CustomerAdapter(MyApp.customerList, getActivity());
        listView.setAdapter(customerAdapter);


    }
}
