package xiangmeila.xiang.com.flower;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
public class FragmentHome extends Fragment {
    ListView listView;
    View view;
    FlowerAdapter flowerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        initData();
        return view;
    }

    SqlHelper dbHelper;

    /**
     * 初始化
     */
    private void initData() {
        dbHelper = new SqlHelper(getActivity());
        listView = view.findViewById(R.id.lv_home);
        //**给列表数据
        flowerAdapter = new FlowerAdapter(MyApp.flowerList, getActivity());
        listView.setAdapter(flowerAdapter);
        /**点击事件*/
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//            }
//        });
        /**
         * 长按事件
         */
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                DialogUtils.showDialog(getActivity(), "提示", "请进行操作", "查看", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getActivity(), FlowerDetail.class);
                        intent.putExtra("detail", MyApp.flowerList.get(position).getFunction() + MyApp.flowerList.get(position).getCultivation());
                        startActivity(intent);

                    }
                }, "种植", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Flower flower = MyApp.flowerList.get(position);
                        /**
                         * 判断是否种植并种植
                         */
                        if(!isCultivation(flower.getName())){
                            add(flower);
                            Toast.makeText(getActivity(), "种植成功", Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(getActivity(), "你已经种植", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                return false;
            }
        });
    }

    /*
   添加已种植
     */
    public void add(Flower flower) {
        //获取写数据库
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //生成要修改或者插入的键值
        ContentValues cv = new ContentValues();
        cv.put(SqlHelper.FLOWER_NAME, flower.getName());
        cv.put(SqlHelper.WATER_TIME , TimeUtils.todayYyyyMmDd());
        cv.put(SqlHelper.NEXT_WATER, TimeUtils.getNext(flower.getWater()));
        // insert 操作
        db.insert(SqlHelper.TABLE_NAME, null, cv);
        //关闭数据库
        db.close();
    }

    /**
     * 判断是否种植
     * @param name
     * @return
     */
    public boolean isCultivation(String name) {
        boolean retuanFlag = false;
        //生成条件语句
        StringBuffer whereBuffer = new StringBuffer();
        whereBuffer.append(SqlHelper.FLOWER_NAME).append(" = ").append("'").append(name).append("'");
        //指定要查询的是哪几列数据
        String[] columns = {SqlHelper.FLOWER_NAME};
        //获取可读数据库
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        //查询数据库
        Cursor cursor = null;
        try {
            cursor = db.query(SqlHelper.TABLE_NAME, columns, whereBuffer.toString(), null, null, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                retuanFlag = true;

                cursor.moveToFirst();
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (SQLException e) {
        }
        //关闭数据库
        db.close();
        return retuanFlag;
    }


}
