package xiangmeila.xiang.com.flower;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    EditText ed_account;
    EditText ed_pwd;
    Button bt_submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ed_account=findViewById(R.id.ed_account);
        ed_pwd=findViewById(R.id.ed_pwd);
        bt_submit=findViewById(R.id.bt_submit);
        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String  account= ed_account.getText().toString();
                String  pad= ed_pwd.getText().toString();
            if(TextUtils.isEmpty(account)){
                Toast.makeText(LoginActivity.this, "请输入账号", Toast.LENGTH_SHORT).show();
            }else if(TextUtils.isEmpty(pad)){
                Toast.makeText(LoginActivity.this, "请输入账号", Toast.LENGTH_SHORT).show();
            }else if(!"admin".equals(account)){

                Toast.makeText(LoginActivity.this, "请输入正确的账号", Toast.LENGTH_SHORT).show();
            }else if(!"123456".equals(pad)){

                Toast.makeText(LoginActivity.this, "请输入正确的密码", Toast.LENGTH_SHORT).show();
            }else {
                Intent intent =new Intent(LoginActivity.this,MainActivity.class);
                startActivity(intent);
            }
            }
        });
    }
}
