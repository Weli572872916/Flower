package xiangmeila.xiang.com.flower;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class FragmentUser extends Fragment {
    ListView listView;
    View view;
    TipAdapter tipAdapter;
    List<Flower> list = new ArrayList<>();
    List<Tip> stringList = new ArrayList<>();
    SqlHelper dbHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       view = inflater.inflate(R.layout.fragment_user, container, false);
        initData();
        return view;
    }

    /**
     * 初始化
     */
    private void initData() {
        list.clear();
        stringList.clear();
        dbHelper = new SqlHelper(getActivity());
        listView = view.findViewById(R.id.lv_home_tip);

        stringList.addAll(findAll());
        Log.d("FragmentUser", "stringList:" + stringList);
        for (int i = 0; i < MyApp.flowerList.size(); i++) {
            for (int i1 = 0; i1 < stringList.size(); i1++) {
            if (stringList.get(i1).getName().equals(MyApp.flowerList.get(i).getName())) {
                list.add(MyApp.flowerList.get(i));
            }
        }
        }
        tipAdapter = new TipAdapter(list, stringList,getActivity());
        listView.setAdapter(tipAdapter);
    }

    /**
     * 查询提醒日期
     * @return
     */
    public List<Tip> findAll() {
        List<Tip> persons = null;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (db.isOpen()) {
            persons = new ArrayList<Tip>();
            Cursor cursor = db.query(SqlHelper.TABLE_NAME, null, null, null, null, null, null);
            while (cursor.moveToNext()) {
                Tip tip = new Tip();
                tip.setName(cursor.getString(cursor.getColumnIndex(SqlHelper.FLOWER_NAME)));
                tip.setDate(cursor.getString(cursor.getColumnIndex(SqlHelper.NEXT_WATER)));
                persons.add(tip);
            }
            cursor.close();
            db.close();
        }
        return persons;
    }

}
