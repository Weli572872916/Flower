package xiangmeila.xiang.com.flower;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 *  本地数据类，
 *  @author Weli
 *  @time 2017-11-24  9:40
 *  @describe
 */
public class SqlHelper extends SQLiteOpenHelper {
    private static SqlHelper dbHelper;
    private static final String TAG = "DatabaseHelper";
    private static final String DB_NAME = "test_db";
    //数据库名字
    public static String TABLE_NAME = "flower";
    // 表名
    public static String FIELD_ID = "_id";
    //花名
    public static String FLOWER_NAME = "name";
    // 浇水时间
    public static String  WATER_TIME= "time";
    //下次浇水时间
    public static String  NEXT_WATER= "next_time";
    // 列名
    private static final int DB_VERSION = 1;
    // 数据库版本
    public static SqlHelper getInstance(Context context) {
        //单例模式
        if (dbHelper == null) {
            dbHelper = new SqlHelper(context.getApplicationContext());
        }
        return dbHelper;
    }


    public SqlHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String  sql = "CREATE TABLE " + TABLE_NAME + "(" + FIELD_ID + "  integer primary key autoincrement not null,"+ FLOWER_NAME +" text ," + WATER_TIME + " integer  ,"+NEXT_WATER+" text);";
        db.execSQL(sql);
        Log.d(TAG, "aaaaa");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
