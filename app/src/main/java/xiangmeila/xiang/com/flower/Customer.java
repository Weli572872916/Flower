package xiangmeila.xiang.com.flower;

public class Customer {

    private  String   name;
    private  String  phone;
    private  String   local;
    private  String  orderDetail;
    private String orderMoney;

    public Customer(String name, String phone, String local, String orderDetail, String orderMoney) {
        this.name = name;
        this.phone = phone;
        this.local = local;
        this.orderDetail = orderDetail;
        this.orderMoney = orderMoney;
    }

    public String getOrderMoney() {
        return orderMoney;
    }

    public void setOrderMoney(String orderMoney) {
        this.orderMoney = orderMoney;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(String orderDetail) {
        this.orderDetail = orderDetail;
    }
}
