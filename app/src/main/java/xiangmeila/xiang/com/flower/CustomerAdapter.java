package xiangmeila.xiang.com.flower;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomerAdapter extends BaseAdapter {
    private List<Customer> stuList;
    private LayoutInflater inflater;
    public CustomerAdapter() {
    }
    public CustomerAdapter(List<Customer> stuList, Context context) {
        this.stuList = stuList;
        this.inflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return stuList == null ? 0 : stuList.size();
    }

    @Override
    public Customer getItem(int position) {
        return stuList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //加载布局为一个视图
        View view = inflater.inflate(R.layout.item_order, null);
        Customer student = getItem(position);
        //在view视图中查找id为image_photo的控件
        TextView tv_customer_name = (TextView) view.findViewById(R.id.tv_customer_name);
        TextView tv_customer_phone = (TextView) view.findViewById(R.id.tv_customer_phone);
        TextView tv_customer_local = (TextView) view.findViewById(R.id.tv_customer_local);
        TextView tv_customer_detial = (TextView) view.findViewById(R.id.tv_customer_detial);
        TextView tv_customer_money = (TextView) view.findViewById(R.id.tv_customer_money);
        tv_customer_name .setText(student.getName());
        tv_customer_phone .setText(student.getPhone());
        tv_customer_local .setText(student.getLocal());
        tv_customer_detial.setText(student.getOrderDetail());
        tv_customer_money .setText(student.getOrderMoney());
        return view;
    }
}
