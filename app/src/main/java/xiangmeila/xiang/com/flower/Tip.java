package xiangmeila.xiang.com.flower;

/**
 * Created by Weli on 2018/6/20.
 */

public class Tip {

    private  String name;
    private String  Date;

    public Tip(String name, String date) {
        this.name = name;
        Date = date;
    }

    public Tip() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    @Override
    public String toString() {
        return "Tip{" +
                "name='" + name + '\'' +
                ", Date='" + Date + '\'' +
                '}';
    }
}
