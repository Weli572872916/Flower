package xiangmeila.xiang.com.flower;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Administrator
 * @name FoodServer
 * @class describe
 * @time 2018-03-13 10:22
 */
public class TimeUtils {
    /**
     * @Description:主要功能:时间日期管理
     * @Prject: CommonUtilLibrary
     * @Package: com.jingewenku.abrahamcaijin.commonutil
     * @author: june
     * @date: 2017年05月05日 14:18
     * @Copyright: 个人版权所有
     * @Company:
     * @version: 1.0.0
     */


    public TimeUtils() {
        throw new UnsupportedOperationException("cannot be instantiated");

    }


    public static final SimpleDateFormat YYYYMMDD_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    public static final SimpleDateFormat HHMMSS_FORMAT = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());

    public static final SimpleDateFormat YYYYMMDDHHMMSS_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

    public static final SimpleDateFormat YYYYMMDDHHMM_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());


    /**
     * 当天的年月日
     *
     * @return
     */


    public static String todayYyyyMmDd() {

        return YYYYMMDD_FORMAT.format(new Date());

    }


    /**
     * 当天的时分秒
     *
     * @return
     */


    public static String todayHhMmSs() {
        return HHMMSS_FORMAT.format(new Date());

    }


    /**
     * 当天的年月日时分秒
     *
     * @return
     */


    public static String todayYyyyMmDdHhMmSs() {
        return YYYYMMDDHHMMSS_FORMAT.format(new Date());

    }


     /**
       * 时分秒
       * @param date
       * @return
       */


    public static String parseHhMmSs(Date date) {
         return HHMMSS_FORMAT.format(date);

    }

    public static String todayYyyyMmDdHhMm() {
        return YYYYMMDDHHMM_FORMAT.format(new Date());

    }

  public  static String getYyyyMm(String str){
        if (str==null){
            return "";
        }
      Pattern pattern = Pattern.compile("[0-9]{4}[-][0-9]{1,2}");
      Matcher matcher = pattern.matcher(str);

      String dateStr = null;
      if(matcher.find()){
          dateStr = matcher.group(0);
      }
    return  dateStr.toString();
  }

    /**
     * 得到上个月日期
     * @return
     */
  public  static String getLastMonth(){
      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      Date date = new Date();
      System.out.println("当前时间是：" + dateFormat.format(date));

      Calendar calendar = Calendar.getInstance();
      // 设置为当前时间
      calendar.setTime(date);
      // 设置为上一个月
      calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
      date = calendar.getTime();
      return dateFormat.format(date);
  }


    /**
     * 得到后几天
     * @param i
     * @return
     */
    public  static  String getNext(int i) {
        Calendar calendar2 = Calendar.getInstance();
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");

        calendar2.add(Calendar.DATE, i);
        return sdf2.format(calendar2.getTime());
    }

    /*(
    计算两个日期相差几天
     */
    public static int daysBetween(String smdate,String bdate) throws ParseException{
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(smdate));
        long time1 = cal.getTimeInMillis();
        cal.setTime(sdf.parse(bdate));
        long time2 = cal.getTimeInMillis();
        long between_days=(time2-time1)/(1000*3600*24);

        return Integer.parseInt(String.valueOf(between_days));
    }


}
