package xiangmeila.xiang.com.flower;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

public class FlowerDetail extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flower_detail);
        //得到详情并展示
        String  flowerDetail=getIntent().getStringExtra("detail");
        TextView tv_flower_detial=findViewById(R.id.tv_flower_detial);

//        判断不为空，展示
        if(!TextUtils.isEmpty(flowerDetail)){
            tv_flower_detial.setText(flowerDetail);
        }


    }
}
